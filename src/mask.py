#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 09:40:14 2019

@author: he
"""
import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
import math
from astropy.io import fits
from astropy import wcs

L_Patch = int(2.5/math.tan(math.radians(1.0)))

if L_Patch//2 != 0:
    L_Patch += 1

filename1 = ["/home/abeelen/Planck/maps/HFI_SkyMap_100_2048_R2.00_full.fits",
             "/home/abeelen/Planck/maps/HFI_SkyMap_143_2048_R2.00_full.fits",
             "/home/abeelen/Planck/maps/HFI_SkyMap_217_2048_R2.00_full.fits",
             "/home/abeelen/Planck/maps/HFI_SkyMap_353_2048_R2.00_full.fits",
             "/home/abeelen/Planck/maps/HFI_SkyMap_545_2048_R2.00_full.fits",
             "/home/abeelen/Planck/maps/HFI_SkyMap_857_2048_R2.00_full.fits"]

pccsfile = ["/home/he/ComputerProject/ComputerProject/data/COM_PCCS_100_R2.01.fits",
            "/home/he/ComputerProject/ComputerProject/data/COM_PCCS_143_R2.01.fits",
            "/home/he/ComputerProject/ComputerProject/data/COM_PCCS_217_R2.01.fits",
            "/home/he/ComputerProject/ComputerProject/data/COM_PCCS_353_R2.01.fits",
            "/home/he/ComputerProject/ComputerProject/data/COM_PCCS_545_R2.01.fits",
            "/home/he/ComputerProject/ComputerProject/data/COM_PCCS_857_R2.01.fits"]

"""
sigma: resolution for each frequency
src_select: signal to noisy ration threshold
num_sig = cluster_size used in mask 

"""
sigma = [9.66/2.355, 7.27/2.355, 5.01/2.355, 4.86/2.355, 4.84/2.355, 4.63/2.355]
src_select = [10,10,10,10,5,5]
num_sig = [1,1,1,1,1,1]


def WorldCoor(lon,lat,L_Patch):

    pixel_coor = [[i,j] for i in range(0,L_Patch) for j in range (0,L_Patch)]
    w = wcs.WCS(naxis = 2)
    w.wcs.crpix = [L_Patch/2-1,L_Patch/2-1]
    w.wcs.cdelt = np.array([-math.tan(math.radians(1.0)),math.tan(math.radians(1.0))])
    w.wcs.crval = [lon,lat]
    w.wcs.ctype = ["GLON-TAN","GLAT-TAN"]
    world = w.wcs_pix2world(pixel_coor,1)
    coordinate_map = hp.pixelfunc.ang2pix(2048,world[:,0],world[:,1],lonlat=True)
    
    return coordinate_map

def pccs_mask(frequency):
    
    n = 0
    if frequency == 100:
        n = 0
        #Data_pccs = fits.getdata(pccsfile[n]
    if frequency == 143:
        n = 1
    if frequency == 217:
        n = 2
    if frequency == 353:
        n = 3
    if frequency == 545:
        n = 4
    if frequency == 857:
        n = 5
        
    pccs_lon= []
    pccs_lat= []
    
    Data_pccs = fits.getdata(pccsfile[n])
    pccs_lon0 = Data_pccs["GLON"]
    pccs_lat0 = Data_pccs["GLAT"]
    pccs_sig = Data_pccs['DETFLUX']
    pccs_err = Data_pccs['DETFLUX_ERR']
    snr = pccs_sig/pccs_err
    for s in range(0,len(pccs_lon0)):
        if snr[s] > src_select[n]:
            pccs_lon.append(pccs_lon0[s])
            pccs_lat.append(pccs_lat0[s])
            
    #pccs_coor = hp.pixelfunc.ang2pix(2048,pccs_lon,pccs_lat,lonlat=True)
    pccs_position_vec = hp.ang2vec(pccs_lon,pccs_lat,lonlat = True)
    
    #pccs_pixel_coor = []
    mask = np.ones(hp.nside2npix(2048))
    for i in range(0,len(pccs_position_vec)):
        pixel_coor = hp.query_disc(2048, pccs_position_vec[i], num_sig[n]*sigma[n]*3.14/180./60.)
        
        mask[pixel_coor] = 0
                                   
        #pccs_pixel_coor.append(pixel_coor)
    
    return mask

def galaxy_mask(frequency, threshold, cluster_lon, cluster_lat):
    
    n = 0
    if frequency == 100:
        n = 0
    if frequency == 143:
        n = 1
    if frequency == 217:
        n = 2
    if frequency == 353:
        n = 3
    if frequency == 545:
        n = 4
    if frequency == 857:
        n = 5
    
    map800 = hp.read_map(filename1[5])
    #Data = fits.getdata("/home/abeelen/Planck/PSZ/planck_ESZ_v2.fits")
    #cluster_lon = Data["GLON"]
    #cluster_lat = Data["GLAT"]
    cluster_position_vec = hp.ang2vec(cluster_lon,cluster_lat,lonlat = True)

    
    newmap = np.copy(map800)
    newmap[map800<threshold] = 1.
    newmap[map800>=threshold] = 0.
    

    for p in range(0,len(cluster_position_vec)):  
        cluster_pixel_coor = hp.query_disc(2048, cluster_position_vec[p], num_sig[n]*sigma[n]*3.14/180./60.)
        newmap[cluster_pixel_coor] = 1.
            
    return newmap



def sky_projection(frequency,threshold):

                   
    Data = fits.getdata("/home/abeelen/Planck/PSZ/planck_ESZ_v2.fits")
    cluster_lon = Data["GLON"]
    cluster_lat = Data["GLAT"]
    
    conv_fact = [244.1,371.74,483.690,287.450,1.0,1.0]
    n = 0
    
    if frequency == 100:
        map1 = hp.read_map(filename1[0])
        n = 0
    if frequency == 143:
        map1 = hp.read_map(filename1[1])
        n = 1
    if frequency == 217:
        map1 = hp.read_map(filename1[2])
        n = 2
    if frequency == 353:
        map1 = hp.read_map(filename1[3])
        n = 3
    if frequency == 545:
        map1 = hp.read_map(filename1[4])
        n = 4
    if frequency == 857:
        map1 = hp.read_map(filename1[5]) 
        n = 5
    print(n)
    #mask_map1 = mask_map()
    #map1 = [hp.read_map(file) for file in filename1]
    
    #patch = [np.zeros((L_Patch, L_Patch)) for k in range(0,len(filename1))]
    #print(np.shape(patch))
    patch = np.zeros((L_Patch, L_Patch))
    masked_patch = np.zeros((L_Patch, L_Patch))
    gala_masked_patch = np.zeros((L_Patch, L_Patch))
    count_patch = np.zeros((L_Patch, L_Patch))
    
    pccs_mask1 = pccs_mask(frequency)
    gal_mask = galaxy_mask(frequency,threshold)
    mask1 = pccs_mask1 + gal_mask
    mask1[mask1 == 2.] = 1.
    masked_map1 = pccs_mask1*map1
    masked_map2 = masked_map1*gal_mask
    
    #pixel_coor = [[i,j] for i in range(0,L_Patch) for j in range (0,L_Patch)]
   
    w = wcs.WCS(naxis = 2)
    #for n in range(0,len(filename1)):
    for c in range(0,len(cluster_lon)):
        coordinate_map0 = WorldCoor(cluster_lon[c],cluster_lat[c],L_Patch)
        patch = np.reshape(map1[coordinate_map0]*conv_fact[n],(L_Patch, L_Patch)) + patch
        masked_patch  = np.reshape(masked_map1[coordinate_map0],(L_Patch, L_Patch)) + masked_patch
        
        count_patch = np.reshape(mask1[coordinate_map0],(L_Patch, L_Patch)) + count_patch
        
        gala_masked_patch  = np.reshape(masked_map2[coordinate_map0],(L_Patch, L_Patch)) + gala_masked_patch
    patch = patch/len(cluster_lon)
    count_patch[count_patch == 0.] = 1.
    
    #masked_patch = masked_patch/len(cluster_lon)
    gala_masked_patch = gala_masked_patch/count_patch

    return patch,masked_patch,gala_masked_patch

    



    
    

    

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 14:13:10 2019

@author: fougeres
"""
from astropy import wcs
import numpy as np
import healpy as hp

def building_wcs (Npixel,CenterValue,arcmin_resolution):
        w = wcs.WCS(naxis=2)
        w.wcs.crpix = [Npixel/2,Npixel/2]
        w.wcs.crval = CenterValue
        w.wcs.cdelt = np.array([-np.tan(np.deg2rad(arcmin_resolution/60.0)),np.tan(np.deg2rad(arcmin_resolution/60.0))])
        w.wcs.ctype = ["GLON-TAN","GLAT-TAN"]
        return w
    
def coordinate_transformation(L_patche,Cluster_coord,arcmin_resolution,xx,yy):
     w = building_wcs (L_patche,Cluster_coord,arcmin_resolution)
     lon,lat = w.wcs_pix2world(xx,yy,1)
     coordinate_map= hp.pixelfunc.ang2pix(2048,lon,lat,lonlat=True)
     return coordinate_map
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 14:37:38 2019

@author: fougeres
"""

import numpy as np
from photometry import flux_photo
from photometry import BootstrapPhotometry


def sample_SZ_photometry(list_patche_sample,FWHM,resolution):
    L_patche = len(list_patche_sample[0][0])
    cluster_half_size = (FWHM/(3.0*60.0))/np.tan(np.deg2rad(resolution/60.0)) #in xy pixel
    print(cluster_half_size)
    Patche = np.sum(list_patche_sample,axis=0)
    Flux_photo,coor_temp_cluster, coor_temp_noise = flux_photo(Patche,[int(L_patche/2),int(L_patche/2)],int(cluster_half_size))
    print('Photometry computed')
    flux_photo_boot = BootstrapPhotometry(list_patche_sample,cluster_half_size)
    return Flux_photo,flux_photo_boot,cluster_half_size,coor_temp_cluster, coor_temp_noise
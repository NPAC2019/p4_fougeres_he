#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 11:28:30 2019

@author: fougeres
"""
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
from photometry import SZ_analytic_form_CMB
from photometry import SZ_analytic_form_CIB
from photometry import SZ_fit
from patche_photometry_frequency_paralleled import multicore

plt.ion()

#Sampling

frequencies = [100.0,143.0,217.0,353.0,545.0,857.0]
res_Flux_photo,res_err_photometry,res_coor_cluster ,res_coor_noise,res_patches = multicore(frequencies)
res_Fl = np.reshape(res_Flux_photo,(len(frequencies)))
Fl_mean = np.array(res_err_photometry)[:,0]
err_photo = np.array(res_err_photometry)[:,1]
print(res_Fl)
print(Fl_mean)

SZphotometry_fitted, amplitude_parameter_optimized = SZ_fit(np.array(frequencies),np.reshape(res_Fl,(6,)),err_photo)


X,Xnoise = np.array(res_coor_cluster),np.array(res_coor_noise)
v_min = [np.amin(np.sum(res_patches[m],axis=0)) for m in range(len(res_patches))]
v_max = [np.amax(np.sum(res_patches[m],axis=0)) for m in range(len(res_patches))]

#print(Flux_photo)
#print(cluster_half_size)
print(err_photo)

plt.figure(1)
for f in range(len(frequencies)):
    plt.subplot(1,6,f+1)
    plt.imshow(np.sum(res_patches[f],axis=0),norm =LogNorm(vmin = v_min[f], vmax = v_max[f]))
    plt.scatter(X[f][0],X[f][1],c='red',marker='+')
    plt.scatter(Xnoise[f][0],Xnoise[f][1],c='blue',marker='+')
    plt.title("Frequency = {} GHz".format(frequencies[f]))
plt.figure(2)
freq = np.arange(100,900)
plt.errorbar(frequencies,res_Fl,yerr = err_photo ,fmt='b+')
plt.plot(frequencies,Fl_mean,'r+',label='Bootstrap photometry')
plt.plot(freq,SZphotometry_fitted,'k--',label='SZ fit with Acmb = {},Acib {} and beta={}]'.format(np.round(amplitude_parameter_optimized[0],4),np.round(amplitude_parameter_optimized[1],4),np.round(amplitude_parameter_optimized[2],4)))
plt.plot(freq,SZ_analytic_form_CMB(freq,amplitude_parameter_optimized[0]),'g--',label='SZ fit with Acmb = {}]'.format(np.round(amplitude_parameter_optimized[0],4)))
plt.plot(freq,SZ_analytic_form_CIB(freq,amplitude_parameter_optimized[1],amplitude_parameter_optimized[2]),'m--',label='Grey Body fit with Acib {} and beta={}]'.format(np.round(amplitude_parameter_optimized[1],4),np.round(amplitude_parameter_optimized[2],4)))
plt.grid(True)
plt.xlabel('Frequency (GHz)')
plt.ylabel('Relative photometry')
plt.legend()



#lowest_resolution = hp.pixelfunc.ud_grade(map,2)
#plt.ion()
#hp.mollview(map1,norm="hist")

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 08:58:27 2019

@author: fougeres
"""

import multiprocessing as mp
import matplotlib.pyplot as plt
import numpy as np
from astropy.io import fits
import healpy as hp
from matplotlib.colors import LogNorm
from photometry import flux_photo
from photometry import SZ_analytic_form_CMB
from photometry import SZ_analytic_form_CIB
from photometry import BootstrapPhotometry
from photometry import SZ_fit
from sky_coordinate_transformation import coordinate_transformation
from mask import pccs_mask, galaxy_mask
from SZ_photometry_sample import sample_SZ_photometry

def map_reading(f):  
    filename_list = np.array(["/home/abeelen/Planck/maps/HFI_SkyMap_100_2048_R2.00_full.fits",	
           "/home/abeelen/Planck/maps/HFI_SkyMap_143_2048_R2.00_full.fits",
           "/home/abeelen/Planck/maps/HFI_SkyMap_217_2048_R2.00_full.fits",
           "/home/abeelen/Planck/maps/HFI_SkyMap_353_2048_R2.00_full.fits",
           "/home/abeelen/Planck/maps/HFI_SkyMap_545_2048_R2.00_full.fits",
           "/home/abeelen/Planck/maps/HFI_SkyMap_857_2048_R2.00_full.fits"])
    n = np.arange(0,6)
    frequencies  = np.array([100.0, 143.0, 217.0, 353.0, 545.0, 857.0])
    map1 = hp.read_map(np.str(filename_list[(frequencies==f)][0]))
    return map1, n[(frequencies==f)]

def cluster_sampling(catalog_name, condition_name,condition_type,condition):
    Data = fits.getdata(catalog_name)
    Cluster_lon = Data['GLON']
    Cluster_la = Data['GLAT']
    Cluster_condition_list  = Data[condition_name]
    if condition_type == 'UP':
        cluster_sample = (Cluster_condition_list >= condition)
    if condition_type == 'DOWN':
        cluster_sample = (Cluster_condition_list <= condition)
    if condition_type == 'EQUAL':
        cluster_sample = (Cluster_condition_list == condition)
    return Cluster_lon[cluster_sample], Cluster_la[cluster_sample], cluster_sample


def building_patches(map1,frequency,conver,cluster_lon,cluster_la,Mask_choi):
   resolution =9.0
   L_patche = np.array((0.9/np.tan(np.deg2rad(resolution/60.0))),dtype=int)
   if L_patche%2!=0:
       L_patche+=1
   print(L_patche)
   if Mask_choi == True:
            pccs_mask1 = pccs_mask(frequency)
            galaxy_mask1 = galaxy_mask(frequency,10,cluster_lon,cluster_la)
            f_mask1 = np.zeros((len(map1)))
            f_mask1 = pccs_mask1 + galaxy_mask1
            f_mask1[f_mask1 == 2] = 1.
            map1 = map1*f_mask1
   nCluster = len(cluster_lon)
   print(nCluster)
   patches =[np.zeros((L_patche,L_patche)) for c in range(nCluster)]
   xx,yy= np.indices((L_patche,L_patche))
   """""he"""""""""
   count_patch = np.zeros((L_patche, L_patche))
   """"""
   print("frequencies {}".format(frequency))
   for c in range(nCluster):
                coordinate_map = coordinate_transformation(L_patche,[cluster_lon[c],cluster_la[c]],resolution,xx,yy)
                patches[c] = np.reshape(map1[coordinate_map],(L_patche,L_patche))*conver
   if Mask_choi == True:
          """""he"""""""""
          count_patch = np.reshape(f_mask1[coordinate_map],(L_patche,L_patche)) + count_patch
          count_patch[count_patch == 0.] = 1.
          patches = patches/count_patch
   if Mask_choi == False:
           patches= np.array(patches)/np.float(nCluster)
   return patches
   
        
def job(frequency):
    resolution =9.0
    mask_choi = True
    conversion = np.array([244.1,317.74,483.690,287.450,1.0,1.0])
    frequencies = np.array([100.0,143.0,217.0,353.0,545.0,857.0])
    FWHM = np.array([9.66,7.27,5.01,4.86,4.84,4.63])
    conver,fwhm = conversion[(frequencies==frequency)], FWHM[(frequencies==frequency)]
    Cluster_lon,Cluster_la, Cluster_sample =  cluster_sampling("/home/abeelen/Planck/PSZ/PSZ2v1.fits", 'Q_Neural','UP', 0.7)
    map1, n = map_reading(frequency)
    patches = building_patches(map1,frequency,conver[0],Cluster_lon,Cluster_la,mask_choi)
    Flux_photo,flux_photo_boot,cluster_half_size,coor_cluster,coor_noise  = sample_SZ_photometry(patches,fwhm[0],resolution)     



    return Flux_photo,flux_photo_boot,cluster_half_size,coor_cluster,coor_noise, patches

def multicore(frequencies):
    pool = mp.Pool()
    result = pool.map(job,frequencies)

    res_Flux_photo = [result[i][0] for i in range(len(frequencies))]
    res_flux_boot = [result[i][1] for i in range(len(frequencies))]
    res_coor_cluster = [result[i][3] for i in range(len(frequencies))]
    res_coor_noise = [result[i][4] for i in range(len(frequencies))]
    res_patches = [result[i][5] for i in range(len(frequencies))]


    return res_Flux_photo,res_flux_boot,res_coor_cluster ,res_coor_noise,res_patches 

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 16:20:01 2019

@author: he
"""

import multiprocessing as mp
import matplotlib.pyplot as plt
import numpy as np
from astropy.io import fits
import healpy as hp
from matplotlib.colors import LogNorm
from photometry import flux_photo
from photometry import SZ_analytic_form_CMB
from photometry import SZ_analytic_form_CIB
from photometry import BootstrapPhotometry
from photometry import SZ_fit
from sky_coordinate_transformation import coordinate_transformation
from mask import pccs_mask, galaxy_mask


def job(frequency):
    
    mask_choi = True
    conversion = [244.1,317.74,483.690,287.450,1.0,1.0]
    frequencies = [100.0,143.0,217.0,353.0,545.0,857.0]
    FWHM = [9.66,7.27,5.01,4.86,4.84,4.63]

    Data = fits.getdata("/home/abeelen/Planck/PSZ/planck_ESZ_v2.fits")
    Cluster_lon = Data['GLON']
    Cluster_la = Data['GLAT']
    nCluster = len(Cluster_lon)
    
    n = 0
    if frequency == 100.:
        n = 0
        filename =["/home/abeelen/Planck/maps/HFI_SkyMap_100_2048_R2.00_full.fits"]	
    if frequency == 143.:
        n = 1
        filename =["/home/abeelen/Planck/maps/HFI_SkyMap_143_2048_R2.00_full.fits"]
    if frequency == 217:
        n = 2
        filename =["/home/abeelen/Planck/maps/HFI_SkyMap_217_2048_R2.00_full.fits"]
    if frequency == 353:
        n = 3
        filename =["/home/abeelen/Planck/maps/HFI_SkyMap_353_2048_R2.00_full.fits"]
    if frequency == 545:
        n = 4
        filename =["/home/abeelen/Planck/maps/HFI_SkyMap_545_2048_R2.00_full.fits"]
    if frequency == 857:
        n = 5
        filename =["/home/abeelen/Planck/maps/HFI_SkyMap_857_2048_R2.00_full.fits"]
    
    print(len(Cluster_lon))


#L_patche = int(np.floor(0.9/np.tan(np.deg2rad(arcmin_resolution/60.0))))
#if L_patche//2 !=0:
 #   L_patche +=1
    L_patche = np.array((1.5/np.tan(np.deg2rad(np.array(FWHM)/60.0))),dtype=int)
    L_patche[L_patche%2!=0] +=1

    map1 =[hp.read_map(file) for file in filename]

   
    pccs_mask1 = [pccs_mask(frequencies[n])]
    galaxy_mask1 = [galaxy_mask(frequencies[n],10)]
    f_mask1 = [np.zeros((len(map1[0]))) for k in range(len(filename))]
    for i in range(len(filename)):
       f_mask1[i] = pccs_mask1[i] + galaxy_mask1[i]
       #arr = f_mask1[i]
       f_mask1[i][f_mask1[i] == 2] = 1.
       map1[i] = map1[i]*f_mask1[i]


    Flux_photo,cluster_half_size = np.zeros((len(filename),1)),np.zeros((len(filename),1))
    patches =[[np.zeros((L_patche[n],L_patche[n])) for c in range(nCluster)] for k in range(len(filename))]
    coor_noise, coor_cluster = [],[]
    #xx,yy= np.indices((L_patche,L_patche))
    for m in range(len(filename)):
       xx,yy= np.indices((L_patche[n],L_patche[n]))
       """""he"""""""""
       count_patch = np.zeros((L_patche[n], L_patche[n]))
       """"""
       print("frequencies {}".format(frequencies[n]))
       for c in range(nCluster):
           coordinate_map = coordinate_transformation(L_patche[n],[Cluster_lon[c],Cluster_la[c]],FWHM[n],xx,yy)
           patches[m][c] = np.reshape(map1[m][coordinate_map],(L_patche[n],L_patche[n]))*conversion[n]
           """""he"""""""""
           count_patch = np.reshape(f_mask1[m][coordinate_map],(L_patche[n],L_patche[n])) + count_patch
       count_patch[count_patch == 0.] = 1.
       cluster_half_size[m] = (FWHM[n]/(3.0*60.0))/np.tan(np.deg2rad(FWHM[n]/60.0)) #in xy pixel
       if mask_choi:
           patches[m] = patches[m]/count_patch
           Patche = np.sum(patches[m],axis=0)
       else:
           patches[m] = np.array(patches[m])/np.float(nCluster)
           Patche = np.sum(patches[m],axis=0)
   
       Flux_photo[m],coor_temp_cluster, coor_temp_noise = flux_photo(Patche,[int(L_patche[n]/2),int(L_patche[n]/2)],int(cluster_half_size[m]))
       print('Photometry computed')
       coor_cluster = coor_cluster + [coor_temp_cluster]
       coor_noise = coor_noise + [coor_temp_noise]



    X,Xnoise = np.array(coor_cluster),np.array(coor_noise)
                  
    v_min = [np.amin(np.sum(patches[m],axis=0)/np.float(nCluster)) for m in range(len(patches))]
    v_max = [np.amax(np.sum(patches[m],axis=0)/np.float(nCluster)) for m in range(len(patches))]
    flux_photo_boot = [BootstrapPhotometry(patches[f],cluster_half_size[f]) for f in range(len(filename))]

    #SZphotometry_fitted, amplitude_parameter_optimized = SZ_fit(np.array(frequencies),np.reshape(Flux_photo,(6,)),np.array(flux_photo_boot)[:,1])
    err_photometry = np.array(flux_photo_boot)[:,1]
    flux_boot = np.array(flux_photo_boot)[:,0]
    print(Flux_photo)
    print(cluster_half_size)
    print(err_photometry)

    return Flux_photo,cluster_half_size,err_photometry,flux_boot

def multicore(frequencies):
    #frequencies = [100.0,143.0]
    pool = mp.Pool()
    result = pool.map(job,frequencies)
    
    res_Flux_photo = [result[i][0] for i in range(len(frequencies))]
    res_err_photometry = [result[i][2] for i in range(len(frequencies))]
    res_Flux_photo = np.reshape(res_Flux_photo,(len(frequencies)))
    res_err_photometry = np.reshape(res_err_photometry,(len(frequencies)))
    res_flux_boot = [result[i][3] for i in range(len(frequencies))]
    res_flux_boot = np.reshape(res_flux_boot,(len(frequencies)))
    
    SZphotometry_fitted, amplitude_parameter_optimized = SZ_fit(np.array(frequencies),res_Flux_photo,res_err_photometry)
    
    print(res_Flux_photo)
    print(res_err_photometry)
    print(res_flux_boot)
 
    return res_Flux_photo, res_err_photometry, res_flux_boot, SZphotometry_fitted, amplitude_parameter_optimized


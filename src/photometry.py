#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 13:55:03 2019

@author: fougeres
"""
import numpy as np
from random import randrange
import scipy.optimize as op

#Physical_constant 
h = 6.62*10**(-34) #J.s
kT_cmb = 2.7*1.38*10**(-23) #J
kT_cib = 18.5*1.38*10**(-23)
c = 3.0*10**8

def flux_photo(image, central_value_position,cluster_size):
    XX,YY = np.indices((len(image),len(image[0])))
    distance = np.sqrt((XX-central_value_position[0])**2+(YY-central_value_position[1])**2)-cluster_size
    distance_noise = np.sqrt((XX-central_value_position[0])**2+(YY-central_value_position[1])**2)-3.5*cluster_size
    mask_coordinate_cluster = (abs(distance-cluster_size)<0.05)
    mask_coordinate_background = (abs(distance_noise-3.5*cluster_size)<0.05)
    coordinate_cluster = [XX[mask_coordinate_cluster],YY[mask_coordinate_cluster]]
    coordinate_noise = [XX[mask_coordinate_background],YY[mask_coordinate_background]]
    mask_cluster = (distance<cluster_size)
    mask_background = ((distance>=cluster_size) & (distance_noise<=3.5*cluster_size))
    cluster = np.array(image)[mask_cluster]
    background = np.array(image)[mask_background]
    return np.sum(cluster)/len(cluster) - np.sum(background)/len(background), coordinate_cluster,coordinate_noise

def SZ_analytic_form(xdata,Acmb,Acib,beta):
    x= [(h*xdata[i]*10**9)/kT_cmb for i in range(len(xdata))]
    y = [(h*xdata[i]*10**9)/kT_cib for i in range(len(xdata))] 
    g = [(10**17)*((((2*h*(xdata[i]**3)*(10**27))*(x[i]*np.exp(x[i])))/((c**2)*((np.exp(x[i])-1)**2)))*(x[i]*(np.exp(x[i])+1)/(np.exp(x[i])-1)-4)*Acmb) + (10**14)*(Acib*((xdata[i]/100.0)**beta)*(2*h*(xdata[i]**3)*(10**27))/((c**2)*(np.exp(y[i])-1))) for i in range(len(xdata))]
    return np.array(np.float64(g))

def SZ_fit(xdata,ydata,yerr):
    print("fit beginnning")
    fit_parameter, fit_parameter_cov = op.curve_fit(SZ_analytic_form,xdata,ydata,p0=[1.0,0.1,1.5],sigma=yerr)
    fit_SZ = SZ_analytic_form(np.arange(100,900),fit_parameter[0],fit_parameter[1],fit_parameter[2])
    return fit_SZ,fit_parameter

def BootstrapPhotometry(patche_cluster_list, cluster_half_size):
    print("Bootstrap error computation")
    L_cluster,l_patch = len(patche_cluster_list),len(patche_cluster_list[0])
    N = 20
    Flux_photo = np.zeros((N,1))
    new_patch = [np.zeros((len(patche_cluster_list[0]),len(patche_cluster_list[0]))) for p in range (N)]
    for p in range (N):
        a = [patche_cluster_list[randrange(0,L_cluster)] for c in range(L_cluster)]
        new_patch[p] = np.sum(a,axis=0)
        Flux_photo[p],coor_temp_cluster, coor_temp_noise = flux_photo(new_patch[p],[int(l_patch/2),int(l_patch/2)],int(cluster_half_size))
    mean = np.sum(new_patch,axis=0)/N
    Flux_photo_mean,coor_temp_cluster, coor_temp_noise = flux_photo(mean,[int(l_patch/2),int(l_patch/2)],int(cluster_half_size))
    Flux_photo_std = np.sqrt(np.sum([(Flux_photo[p]-Flux_photo_mean)**2 for p in range(N)])/N)
    return Flux_photo_mean,Flux_photo_std     

def SZ_analytic_form_CMB(xdata,Acmb):
    x= [(h*xdata[i]*10**9)/kT_cmb for i in range(len(xdata))]
    g = [(10**17)*((((2*h*(xdata[i]**3)*(10**27))*(x[i]*np.exp(x[i])))/((c**2)*((np.exp(x[i])-1)**2)))*(x[i]*(np.exp(x[i])+1)/(np.exp(x[i])-1)-4)*Acmb) for i in range(len(xdata))]
    return np.array(np.float64(g))

def SZ_analytic_form_CIB(xdata,Acib,beta):
    y = [(h*xdata[i]*10**9)/kT_cib for i in range(len(xdata))] 
    g = [(10**14)* (Acib*((xdata[i]/100.0)**beta)*(2*h*(xdata[i]**3)*(10**27))/((c**2)*(np.exp(y[i])-1))) for i in range(len(xdata))]
    return np.array(np.float64(g))
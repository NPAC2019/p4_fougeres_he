#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 13:39:03 2019

@author: he
"""

import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
from astropy.io import fits
from astropy import wcs
from mask import sky_projection

from matplotlib.colors import LogNorm
from  matplotlib import colors


plt.ion()

#plt.plot(Cluster_lon,Cluster_la,'ro')
# = pccs_pixel_coordinate(100)

threshold = 10. #threshold for galaxy mask


patch1,pccs_masked_patch1,masked_patch1 = sky_projection(100,threshold)
patch2,pccs_masked_patch2,masked_patch2 = sky_projection(143,threshold)
patch3,pccs_masked_patch3,masked_patch3 = sky_projection(217,threshold)
patch4,pccs_masked_patch4,masked_patch4 = sky_projection(353,threshold)
patch5,pccs_masked_patch5,masked_patch5 = sky_projection(545,threshold)
patch6,pccs_masked_patch6,masked_patch6 = sky_projection(857,threshold)


#vmin = min(patch1[i].min() for i in range (0,len(patch1)))
#vmax = max(patch1[i].max() for i in range (0,len(patch1)))
#norm = colors.Normalize(vmin,vmax)

plt.figure()

"""

plt.subplot(1,3,1)
plt.title('f = 100hz')
plt.imshow(patch1)
#plt.scatter(mask_coor,c= 'red',marker = '+')

plt.subplot(1,3,2)
plt.imshow(masked_patch1)

plt.subplot(1,3,3)
plt.imshow(gala_masked_patch1)

"""

plt.subplot(2,6,1)
plt.title('100GHz')
plt.imshow(patch1)
#plt.scatter(mask_coor,c= 'red',marker = '+')
plt.subplot(2,6,7)
plt.title('100GHz, masked')
plt.imshow(masked_patch1)

plt.subplot(2,6,2)
plt.title('143GHz')
plt.imshow(patch2)
#plt.scatter(mask_coor,c= 'red',marker = '+')
plt.subplot(2,6,8)
plt.title('143GHz, masked')
plt.imshow(masked_patch2)

plt.subplot(2,6,3)
plt.title('217GHz')
plt.imshow(patch3)
#plt.scatter(mask_coor,c= 'red',marker = '+')
plt.subplot(2,6,9)
plt.title('217GHz, masked')
plt.imshow(masked_patch3)

plt.subplot(2,6,4)
plt.title('353GHz')
plt.imshow(patch4)
#plt.scatter(mask_coor,c= 'red',marker = '+')
plt.subplot(2,6,10)
plt.title('353GHz,masked')
plt.imshow(masked_patch4)

plt.subplot(2,6,5)
plt.title('545GHz')
plt.imshow(patch5)
#plt.scatter(mask_coor,c= 'red',marker = '+')
plt.subplot(2,6,11)
plt.title('545GHz,masked')
plt.imshow(masked_patch5)

plt.subplot(2,6,6)
plt.title('857GHz')
plt.imshow(patch6)
#plt.scatter(mask_coor,c= 'red',marker = '+')
plt.subplot(2,6,12)
plt.title('857GHz,masked')
plt.imshow(masked_patch6)







    
    
    
#print(Cluster_lon)